FROM node:12.18.3

MAINTAINER Hedgalex

EXPOSE 3000 3000

COPY script.sh /
RUN mkdir /var/project

CMD [ "bash", "script.sh" ]