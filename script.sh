#!/bin/bash
set -e

cd /var/project
npm i
npm i knex -g
cd knex

knex migrate:latest
knex seed:run

npm start