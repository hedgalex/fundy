import React from 'react'
import ReactDOM from 'react-dom'
import App from './app/App'
import 'semantic-ui-css/semantic.min.css'
import './resources/style.css'

/** @typedef {object} __INITIAL_STATE__ */
const initialData = window.__INITIAL_STATE__ || {}
const user = initialData.user || false

ReactDOM.render(
	<App user={user} />,
	document.getElementById('app')
)