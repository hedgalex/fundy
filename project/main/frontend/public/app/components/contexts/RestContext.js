import React, { useReducer } from 'react'

const RestContext = React.createContext(false)
export default RestContext
export const RestConsumer = RestContext.Consumer

const REQUEST_SIGN_IN  = 'login', REQUEST_SIGN_UP = 'registration'
const PATHS = {
	AUTH: '/auth',
	REGISTRATION: '/registration',
	LOGOUT: '/logout'
}

const _fetch = async (url, method, data) => {
	let config = {
		method: method,
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
	}

	if(data) config.body = JSON.stringify(data)
	let response = await fetch(`/rest${url}`, config)

	return await response.json()
}

// const _get = (url, data) => _fetch(url, 'GET', data)
const _post = (url, data) => _fetch(url, 'POST', data)
// const _put = (url, data) => _fetch(url, 'PUT', data)
// const _delete = (url, data) => _fetch(url, 'DELETE', data)
// const _head = (url, data) => _fetch(url, 'HEAD', data)

export const restReducer = (state, action) => {
	let {type, data, response, complete, error} = action
	switch (type) {
	case REQUEST_SIGN_IN:
		_post(PATHS.AUTH, data).then(response).catch(error).finally(complete)
		return state
	case REQUEST_SIGN_UP:
		_post(PATHS.REGISTRATION, data).then(response).catch(error).finally(complete)
		return state
	default:
		return state
	}
}

class RestRequest {

	constructor(dispatch, type) {
		this._onResponse = () => {}
		this._onComplete = () => {}
		this._onError = () => {}
		this._dispatch = dispatch
		this._type = type
	}

	data = (data) => {
		this._data = data
		return this
	}

	onResponse = (callback) => {
		this._onResponse = callback
		return this
	}

	onComplete = (callback) => {
		this._onComplete = callback
		return this
	}

	onError = (callback) => {
		this._onError = callback
		return this
	}

	handler = (object) => {

		if(!object) {
			console.warn('Handlers not found')
			return this.send()
		}

		if(object.onResponse) this.onResponse(object.onResponse)
		if(object.onComplete) this.onComplete(object.onComplete)
		if(object.onError) this.onError(object.onError)

		return this.send()
	}

	send = () => {
		this._dispatch({type: this._type, data: this._data, response: this._onResponse, complete: this._onComplete, error: this._onError})
	}

}

class Rest {

	constructor(state, dispatch) {
		this.state = state
		this.dispatch = dispatch
	}

	login = (data) => {
		return new RestRequest(this.dispatch, REQUEST_SIGN_IN).data(data)
	}

	signUp = (data) => {
		return new RestRequest(this.dispatch, REQUEST_SIGN_UP).data(data)
	}

}

export function RestProvider(props) {

	const [state, dispatch] = useReducer(restReducer, {})

	return (
		<RestContext.Provider value={new Rest(state, dispatch)}>
			{props.children}
		</RestContext.Provider>
	)
}

// rest.login(data).onResponse(this.onResponse).onComplete(this.onComplete).onError(this.onError).send()
