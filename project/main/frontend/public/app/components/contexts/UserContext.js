import React, {useReducer} from 'react'

const UserContext = React.createContext(false)

export default UserContext
export const UserConsumer = UserContext.Consumer
const LOGIN = 'login', LOGOUT = 'logout'

const userReducer = (state, data) => {
	const {action, user} = data

	switch (action) {
	case LOGIN:
		if(user) return {user: user}
		return state
	case LOGOUT:
		return {user: false}
	default:
		return state
	}
}

class User {

	constructor(state, dispatch) {
		this.state = state
		this.dispatch = dispatch
		this.login = this.login.bind(this)
		this.logout = this.logout.bind(this)
	}

	login = (user) => this.dispatch({action: LOGIN, user: user})

	logout = () => this.dispatch({action: LOGOUT})
}

export function UserProvider(props) {

	const [state, dispatch] = useReducer(userReducer, {user: props.user})

	return (
		<UserContext.Provider value={new User(state, dispatch)}>
			{props.children}
		</UserContext.Provider>
	)
}