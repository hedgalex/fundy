import React, { useReducer } from 'react'

const HeaderContext = React.createContext(false)

export default HeaderContext
export const HeaderConsumer = HeaderContext.Consumer
const CLOSE = 'close', SIGN_IN = 'signin', SIGN_IN_UP = 'signin-up', SIGN_UP = 'signup', SIGN_UP_IN = 'signup-in'

const headerReducer = (state, data) => {
	let {action, dispatch} = data
	if(action === SIGN_IN) {
		switch (state.mode) {
		case CLOSE:
			return {mode: SIGN_IN}
		case SIGN_IN:
			return {mode: CLOSE}
		case SIGN_UP:
			setTimeout(() => {dispatch({action: SIGN_IN, dispatch})}, 300)
			return {mode: SIGN_UP_IN}
		case SIGN_UP_IN:
			return {mode: SIGN_IN}
		}
		return state
	}

	if(action === SIGN_UP) {
		switch (state.mode) {
		case CLOSE:
			return {mode: SIGN_UP}
		case SIGN_UP:
			return {mode: CLOSE}
		case SIGN_IN:
			setTimeout(() => {dispatch({action: SIGN_UP, dispatch})}, 300)
			return {mode: SIGN_IN_UP}
		case SIGN_IN_UP:
			return {mode: SIGN_UP}
		}
		return state
	}

	if(action === CLOSE) return {mode: CLOSE}
}

class Header {

	constructor(state, dispatch) {
		this.state = state
		this.dispatch = dispatch
		this.openSignIn = this.openSignIn.bind(this)
		this.openSignUp = this.openSignUp.bind(this)
		this.close = this.close.bind(this)
	}

	openSignIn = () => this.dispatch({action: SIGN_IN, dispatch: this.dispatch})

	openSignUp = () => this.dispatch({action: SIGN_UP, dispatch: this.dispatch})

	close = () => this.dispatch({action: CLOSE})

}

export function HeaderProvider(props) {

	const [state, dispatch] = useReducer(headerReducer, {mode: CLOSE})

	return (
		<HeaderContext.Provider value={new Header(state, dispatch)}>
			{props.children}
		</HeaderContext.Provider>
	)
}
