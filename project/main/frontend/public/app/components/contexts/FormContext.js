import React from 'react'

const FormContext = React.createContext(null)

export default FormContext
export const FormProvider = FormContext.Provider
export const FormConsumer = FormContext.Consumer