import React, { useReducer } from 'react'

const MessageContext = React.createContext(false)
export default MessageContext
export const MessageConsumer = MessageContext.Consumer

const ADD = 'add', CLOSE = 'close', CLOSE_ALL = 'close_all'
export const MessageType = {INFO: 'info', ERROR: 'negative', WARN: 'warning', SUCCESS: 'positive'}

export const messageReducer = (state, data) => {
	const { id, action } = data
	const { messages } = state
	switch (action) {
	case ADD:
		messages.push(data)
		return {messages: messages}
	case CLOSE:
		const filteredMessages = messages.filter(item => item.id !== id)
		return {messages: filteredMessages}
	case CLOSE_ALL:
		return {messages: []}
	}
}

class Message {

	constructor(state, dispatch) {
		this.state = state
		this.dispatch = dispatch
		this.counter = 0
	}

	clear = (id) => {
		if(typeof id === 'undefined') {
			this.counter = 0
			return this.dispatch({action: CLOSE_ALL})
		}
		this.dispatch({action: CLOSE, id: id})
	}

	error = (text, header, timer) => {
		const id = ++this.counter
		this.dispatch({action: ADD, id: id, type: MessageType.ERROR, header: header, message: text, timer: timer})
		return id
	}

	warn = (text, header, timer) => {
		const id = ++this.counter
		this.dispatch({action: ADD, id: id, type: MessageType.WARN, header: header, message: text, timer: timer})
		return id
	}

	success = (text, header, timer) => {
		const id = ++this.counter
		this.dispatch({action: ADD, id: id, type: MessageType.SUCCESS, header: header, message: text, timer: timer})
		return id
	}

	info = (text, header, timer) => {
		const id = ++this.counter
		this.dispatch({action: ADD, id: id, type: MessageType.INFO, header: header, message: text, timer: timer})
		return id
	}
}

export function MessageProvider(props) {

	const [state, dispatch] = useReducer(messageReducer, {messages: []})

	return (
		<MessageContext.Provider value={new Message(state, dispatch)}>
			{props.children}
		</MessageContext.Provider>
	)
}