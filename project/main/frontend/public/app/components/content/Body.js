import React from 'react'

export default class Body extends React.Component {
	render = () => <div>{this.props.children}</div>
}