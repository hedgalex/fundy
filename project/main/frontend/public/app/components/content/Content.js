import React from 'react'
import ProgressTile from '../main/ProgressTile'

class Content extends React.Component {

	constructor (props) {
		super(props)
	}

	render () {
		return (
			<main>
				<div className="main-inner">
					{this.props.children}
				</div>
			</main>
		)
	}
}

export default Content