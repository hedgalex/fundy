import React from 'react'
import { Button, Form } from 'semantic-ui-react'
import FormInput from '../../form/FormInput'
import FormCheckbox from '../../form/FormCheckbox'
import FormValidator from '../../form/FormValidator'
import { NOT_EMPTY } from '../../libs/Validator'
import { Link } from 'react-router-dom'
import RestReactComponent from '../../libs/RestReactComponent'
import HeaderContext from '../contexts/HeaderContext'

export default class LoginForm extends RestReactComponent {

	constructor (props) {
		super(props)
		this.useContexts(HeaderContext)
	}

	onResponse (response) {
		super.onResponse(response)
		if(response.error) return this.contexts.message.error(response.message, 'Error', 10)

		const { user, header } = this.contexts
		header.close()
		// setTimeout(() => user.login(response.message), 300)
	}

	onSubmit (event, props, data) {
		super.onSubmit(event, props, data)
		const { rest } = this.contexts

		rest.login(data).handler(this)
	}

	html () {

		let submitButtonClassName = 'uppercase'
		if(this.state.loading) submitButtonClassName = `${submitButtonClassName} loading`

		return (
			<FormValidator onSubmit={this.onSubmit}>
				<Form.Field>
					<FormInput name="username" iconPosition="left" icon="mail" fluid placeholder="Username/Email" validate={{NOT_EMPTY}} />
				</Form.Field>
				<Form.Field>
					<FormInput name="password" type="password" iconPosition="left" icon="lock" fluid placeholder="Password" validate={{NOT_EMPTY}} />
				</Form.Field>
				<Form.Field>
					<FormCheckbox name="remember" label="Remember" />
				</Form.Field>
				<p className="submit">
					<Button primary type="submit" className={submitButtonClassName}>Sign in</Button>
					<Link className="forgot-password" to="/forgot">Forgot password?</Link>
				</p>
			</FormValidator>
		)
	}
}