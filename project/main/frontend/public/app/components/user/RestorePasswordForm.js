import React from 'react'
import { Button, Form } from 'semantic-ui-react'
import FormInput from '../../form/FormInput'
import FormCheckbox from '../../form/FormCheckbox'
import FormValidator from '../../form/FormValidator'
import { NOT_EMPTY } from '../../libs/Validator'
import FormMessage from '../../form/FormMessage'
import { REQUEST_SIGN_IN, RestConsumer } from '../contexts/RestContext'
import { ACTION_LOGIN } from '../../App'
import { MessageAction, MessageConsumer } from '../contexts/MessageContext'

class RestorePasswordForm extends React.Component {

	/**
	 * @param props
	 * @param props.userContext
	 * */
	constructor (props) {
		super(props)

		this.state = {
			loading: false,
			message: false
		}
		this.onSubmit = this.onSubmit.bind(this)
		this.onResponse = this.onResponse.bind(this)
		this.onFinal = this.onFinal.bind(this)
	}

	onResponse (response) {
		// if(response.error) return this.setState({message: response.message})
		// if(!this.props.userContext || !this.props.userContext.dispatch) return console.error('Login: UserContext dispatch not found')
		// const {dispatch} = this.props.userContext
		// dispatch({type: ACTION_LOGIN, user: response.message})
	}

	onSubmit (event, props, data) {
		// const {rest} = this.props.restContext
		// this.setState({loading: true})
		// rest({type: REQUEST_SIGN_IN, data: data, response: this.onResponse, final: this.onFinal})
	}

	onFinal () {
		this.setState({loading: false})
	}

	render () {
		let submitButtonClassName = 'uppercase'
		if(this.state.loading) submitButtonClassName = `${submitButtonClassName} loading`
		// const {message} = this.props.messageConsumer
		// message({type: MessageAction.ADD, message: 'Help', header: 'Header', timer: 3000})
		return (
			<FormValidator onSubmit={this.onSubmit}>
				<FormMessage message={this.state.message} />
				<Form.Field>
					<FormInput name="username" iconPosition="left" icon="mail" fluid placeholder="Username/Email" validate={{NOT_EMPTY}} />
				</Form.Field>
				<p className="submit">
					<Button primary type="submit" className={submitButtonClassName}>Restore Password</Button>
				</p>
			</FormValidator>
		)
	}
}

function RestorePasswordFormWrapper(props) {
	return (
		<MessageConsumer>
			{messageConsumer => (
				<RestConsumer>
					{restContext => (
						<RestorePasswordForm {...props} messageConsumer={messageConsumer} restContext={restContext}>{props.children}</RestorePasswordForm>
					)}
				</RestConsumer>
			)}
		</MessageConsumer>

	)
}

export default RestorePasswordFormWrapper