import React  from 'react'
import { Button, Form } from 'semantic-ui-react'
import FormInput from '../../form/FormInput'
import { EMAIL, USER } from '../../libs/Validator'
import FormValidator from '../../form/FormValidator'
import FormPassword from '../../form/FormPassword'
import RestReactComponent from '../../libs/RestReactComponent'
import HeaderContext from '../contexts/HeaderContext'

class RegistrationForm extends RestReactComponent {

	constructor (props) {
		super(props)
		this.useContexts(HeaderContext)
	}

	onResponse (response) {
		super.onResponse(response)

		const { header } = this.contexts
		header.close()
		setTimeout(() => {
			//redirect
		}, 300)
	}

	onSubmit (event, props, data) {
		super.onSubmit(event, props, data)

		const { rest } = this.contexts
		rest.signUp(data).handler(this)
	}

	html() {
		let submitButtonClassName = 'uppercase'
		if(this.state.loading) submitButtonClassName = `${submitButtonClassName} loading`

		return (
			<FormValidator onSubmit={this.onSubmit}>
				<Form.Field>
					<FormInput iconPosition="left" icon="mail" name="email" fluid placeholder="Email" validate={{EMAIL}} />
				</Form.Field>
				<Form.Field>
					<FormInput iconPosition="left" icon="user" name="username" fluid placeholder="Username" validate={{USER}} />
				</Form.Field>
				<Form.Field>
					<FormInput iconPosition="left" icon="user" name="nickname" fluid placeholder="Nickname" />
				</Form.Field>
				<Form.Field>
					<FormPassword name="password" />
				</Form.Field>

				<p className="submit">
					<Button primary type="submit" className={submitButtonClassName}>Registration</Button>
				</p>
			</FormValidator>
		)
	}
}

export default RegistrationForm