import React from 'react'

class ProgressTile extends React.Component {

	constructor (props) {
		super(props)
		let progress = props.progress > 100 ? 100: props.progress
		progress = progress < 0 ? 0: progress

		// #ace
		const defaults = {
			backgroundColor: '#2185d0',
			progressColor: '#f4ad4c'
		}

		const cornerRadius = props.cornerRadius || 10
		const	rowLength = props.rowLength || 80

		const cornerLength = cornerRadius * Math.PI / 2
		const rowLengthInPercent = 25 * rowLength / (cornerLength + rowLength)

		this.state = {
			id: `tile${new Date().getTime()}_${Math.floor(Math.random() * 1000)}`,
			progress: progress,
			icon: props.icon || ' ',
			backgroundColor: props.backgroundColor || defaults.backgroundColor,
			progressColor: props.progressColor || defaults.progressColor,
			defaults: defaults,
			rowLengthInPercent: rowLengthInPercent,
			cornerLengthInPercents: 25 - rowLengthInPercent
		}
	}

	render () {

		let a = this.state.progress / 25
		let count = Math.floor(a)
		let d = this.state.progress - count * 25

		let c = d - this.state.rowLengthInPercent
		c = c < 0 ? 0: c

		let cornerProgress = c * 90 / this.state.cornerLengthInPercents
		let rowProgress = d * 100 / this.state.rowLengthInPercent
		rowProgress = rowProgress > 100 ? 100: rowProgress

		let progress1 = count === 0 ? rowProgress: 100
		let progress2 = count === 1 ? rowProgress: (count > 1 ? 100: 0)
		let progress3 = count === 2 ? rowProgress: (count > 2 ? 100: 0)
		let progress4 = count === 3 ? rowProgress: (count > 3 ? 100: 0)

		let cornerProgress1 = count === 0 ? cornerProgress: 90
		let cornerProgress2 = count === 1 ? cornerProgress: (count > 1 ? 90: 0)
		let cornerProgress3 = count === 2 ? cornerProgress: (count > 2 ? 90: 0)
		let cornerProgress4 = count === 3 ? cornerProgress: (count > 3 ? 90: 0)

		return (
			<div className={`fundy tile ${this.state.id}`}>
				<style>{`
							.fundy.tile.${this.state.id} .corner i:after { background: ${this.state.progressColor}; }
							.fundy.tile.${this.state.id} .corner:after { background: ${this.state.backgroundColor}; }
				`}</style>
				<div className="corner corner1"><i style={{transform: `rotateZ(${-90 + cornerProgress1}deg)`}}/></div>
				<div className="corner corner2"><i style={{transform: `rotateZ(${cornerProgress2}deg)`}}/></div>
				<div className="corner corner3"><i style={{transform: `rotateZ(${90 + cornerProgress3}deg)`}}/></div>
				<div className="corner corner4"><i style={{transform: `rotateZ(${-180 + cornerProgress4}deg)`}}/></div>

				<div className="progress-top" style={{background: `linear-gradient(90deg, ${this.state.progressColor} ${progress2}%, ${this.state.backgroundColor} 0)`}} />
				<div className="progress-left" style={{background: `linear-gradient(0deg, ${this.state.progressColor} ${progress1}%, ${this.state.backgroundColor} 0)`}} />
				<div className="progress-right" style={{background: `linear-gradient(180deg, ${this.state.progressColor} ${progress3}%, ${this.state.backgroundColor} 0)`}} />
				<div className="progress-bottom" style={{background: `linear-gradient(270deg, ${this.state.progressColor} ${progress4}%, ${this.state.backgroundColor} 0)`}} />

				<img className="icon" src={this.state.icon}  alt="Math" />
			</div>
		)
	}
}

export default ProgressTile