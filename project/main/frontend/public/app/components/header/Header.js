import React  from 'react'
import Logo from './Logo'
import HeaderUserAction from './HeaderUserAction'
import HeaderMenu from './HeaderMenu'
import HeaderExtented from './HeaderExtented'
import HeaderContext from '../contexts/HeaderContext'
import ReactComponent from '../../libs/ReactComponent'
import UserContext from '../contexts/UserContext'

export default class Header extends ReactComponent {

	constructor(props) {
		super(props);
		this.useContexts(HeaderContext)
	}

	html() {
		const {header} = this.contexts
		const {state} = header

		return (
			<header className={`fundy ${state.mode}`}>
				<div className="fundy header">
					<div className="fundy header-container">
						<Logo/>
						<div className="fundy header-inner">
							<HeaderMenu/>
							<HeaderUserAction/>
						</div>
					</div>
				</div>
				<HeaderExtented/>
			</header>
		)
	}

}