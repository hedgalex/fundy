import React from 'react'
import LoginForm from '../user/LoginForm'
import RegistrationForm from '../user/RegistrationForm'

class HeaderExtented extends React.Component {

	constructor (props) {
		super(props)
	}

	render = () => (
		<>
			<div className="fundy auth-form">
				<div className="fundy auth-animation transition normal">
					<div className="fundy signin">
						<LoginForm />
					</div>
					<div className="fundy signup">
						<RegistrationForm />
					</div>
				</div>
			</div>
			<div className="fundy header-bottom" />
		</>)

}

export default HeaderExtented