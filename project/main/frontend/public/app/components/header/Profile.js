import React from 'react'
import { Dropdown } from 'semantic-ui-react'
import defaultAvatar from '../../../resources/useravatar.png'
import UserContext from '../contexts/UserContext'
import ReactComponent from '../../libs/ReactComponent'

const SIGN_OUT = 'sign-out'
const PROFILE = 'profile'

const options = [
	{ key: 'user', text: 'Account', icon: 'user', value: PROFILE },
	{ key: 'sign-out', text: 'Sign Out', icon: 'sign out', value: SIGN_OUT }
]

class Profile extends ReactComponent {

	constructor (props) {
		super(props)

		this.useContexts(UserContext)
		this.onClick = this.onClick.bind(this)
	}

	avatar() {

	}

	onClick(item, element) {
		switch (element.value) {
		case SIGN_OUT:
			const {signout} = this.props
			if(signout) signout()
			break
		case PROFILE:
			break
		default:
		}

	}

	html () {
		const { user } = this.contexts
		if(!user) return ''

		let {avatar} = user
		if(!avatar) avatar = defaultAvatar
		return (
			<>
				<Dropdown
					trigger={<img className="fundy profile-logo" src={avatar} alt=""/>}
					options={options}
					icon={null}
					onChange={this.onClick}
				/>
			</>
		)
	}
}

export default Profile