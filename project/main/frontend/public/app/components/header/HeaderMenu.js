import React from 'react'
import {Link} from 'react-router-dom'
import ReactComponent from '../../libs/ReactComponent'

export default class HeaderMenu extends ReactComponent {

	constructor (props) {
		super(props)
	}

	html () {
		return <div className="ui fundy header-menu">
			<div className="ui secondary menu">
				<Link className="item active" to="/">Home</Link>
				<Link className="item" to="/friends">Friends</Link>
			</div>
		</div>
	}
}