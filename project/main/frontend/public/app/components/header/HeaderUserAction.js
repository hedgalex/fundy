import React from 'react'
import Profile from './Profile'
import { Button } from 'semantic-ui-react'
import HeaderContext from '../contexts/HeaderContext'
import UserContext from '../contexts/UserContext'
import ReactComponent from '../../libs/ReactComponent'

class HeaderUserAction extends ReactComponent {

	constructor (props) {
		super(props)

		this.useContexts(UserContext, HeaderContext)
		this.onSignOutClick = this.onSignOutClick.bind(this)
	}

	renderProfileMenu() {
		return <>
			<Profile signout={this.onSignOutClick} />
		</>
	}

	renderLoginButtons() {
		const { header } = this.contexts
		return <>
			<Button onClick={header.openSignIn}>LOGIN</Button>
			<Button.Or />
			<Button onClick={header.openSignUp} color="black">SIGN UP</Button>
		</>
	}

	onSignOutClick() {

	}

	html () {
		const {user} = this.contexts
		let html = user.state.user ? this.renderProfileMenu():this.renderLoginButtons()

		return (
			<div className="fundy auth">
				{html}
			</div>
		)
	}
}

export default HeaderUserAction