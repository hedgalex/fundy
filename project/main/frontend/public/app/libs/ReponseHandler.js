class ResponseHandler {

	constructor (response) {
		this.message = ''
		this.error = true
		if(!response) return

		this.error = response.error
		try {
			this.message = JSON.parse(response.message)
		} catch (e) {
			this.message = response.message
		}

	}

	static create(response) {
		return new ResponseHandler(response)
	}

}

export default ResponseHandler