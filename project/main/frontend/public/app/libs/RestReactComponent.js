import React from 'react'
import ReactComponent from './ReactComponent'
import RestContext from '../components/contexts/RestContext'
import UserContext from '../components/contexts/UserContext'
import MessageContext from '../components/contexts/MessageContext'

export default class RestReactComponent extends ReactComponent {

	constructor (props) {
		super(props)

		this.useContexts(RestContext, UserContext, MessageContext)
		this.state = { loading: false }
		this.onSubmit = this.onSubmit.bind(this)
		this.onResponse = this.onResponse.bind(this)
		this.onComplete = this.onComplete.bind(this)
	}

	onResponse (response) {
		if (response.error) return this.setState({ message: response.message })
	}

	onComplete () {
		this.setState({ loading: false })
	}

	onSubmit (event, props, data) {
		this.setState({ loading: true })
	}


}