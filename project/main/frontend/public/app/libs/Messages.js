import React from 'react'
import Message from './Message'
import { MessageConsumer } from '../components/contexts/MessageContext'

class Messages extends React.Component {

	constructor(props) {
		super(props)
	}

	render() {
		return <div className="fundy messages-container">
			<MessageConsumer>
				{messageContext => messageContext.state.messages.map(({id, text, header, timer, type}) => <Message key={id} id={id} type={type} header={header} text={text} timer={timer} />)}
			</MessageConsumer>
		</div>
	}
}

export default Messages