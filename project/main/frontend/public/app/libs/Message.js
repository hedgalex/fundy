import React from 'react'
import { Message as Dialog } from 'semantic-ui-react'
import MessageContext, {MessageType} from '../components/contexts/MessageContext'

class Message extends React.Component {

	static contextType = MessageContext

	constructor(props) {
		super(props)

		this.state = {dissolving: false}
		const {timer} = this.props
		if(timer && String(timer).match(/\d+/)) this.state.timer = timer * 1000

		this.handleDismiss = this.handleDismiss.bind(this)
	}

	renderHeader() {
		const { header } = this.props
		if(header) return <Dialog.Header>{header}</Dialog.Header>
		return ''
	}

	handleDismiss() {
		this.context.clear(this.props.id)
	}

	componentDidMount() {
		const { timer } = this.state
		if(timer) setTimeout(this.handleDismiss, timer)
	}

	render() {
		const {type, text} = this.props

		const types = {
			info: type === MessageType.INFO ? true: null,
			warning: type === MessageType.WARN ? true: null,
			negative: type === MessageType.ERROR ? true: null,
			positive: type === MessageType.SUCCESS ? true: null
		}

		return <Dialog {...types} onDismiss={this.handleDismiss}>
			{this.renderHeader()}
			<p>{text}</p>
		</Dialog>
	}

}

export default Message