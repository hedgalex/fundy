import React from 'react'
import MessageContext, {MessageConsumer} from '../components/contexts/MessageContext'
import RestContext, {RestConsumer} from '../components/contexts/RestContext'
import UserContext, {UserConsumer} from '../components/contexts/UserContext'
import HeaderContext, {HeaderConsumer} from '../components/contexts/HeaderContext'

const renderUserConsumer = (contextClasses, contexts, self) => {
	return <UserConsumer>{value => {
		contexts.user = value
		return renderContext(contextClasses, contexts, self)
	}}
	</UserConsumer>
}

const renderHeaderConsumer = (contextClasses, contexts, self) => {
	return <HeaderConsumer>{value => {
		contexts.header = value
		return renderContext(contextClasses, contexts, self)
	}}
	</HeaderConsumer>
}

const renderMessageConsumer = (contextClasses, contexts, self) => {
	return <MessageConsumer>{value => {
		contexts.message = value
		return renderContext(contextClasses, contexts, self)
	}}
	</MessageConsumer>
}

const renderRestConsumer = (contextClasses, contexts, self) => {
	return <RestConsumer>{value => {
		contexts.rest = value
		return renderContext(contextClasses, contexts, self)
	}}
	</RestConsumer>
}

const renderContext = (contextClasses, contexts, self) => {

	if (contextClasses && contextClasses.length) {

		const contextItem = contextClasses.shift()

		if (contextItem === UserContext) return renderUserConsumer(contextClasses, contexts, self)
		if (contextItem === MessageContext) return renderMessageConsumer(contextClasses, contexts, self)
		if (contextItem === RestContext) return renderRestConsumer(contextClasses, contexts, self)
		if (contextItem === HeaderContext) return renderHeaderConsumer(contextClasses, contexts, self)

	}

	return self.html()
}

export default class ReactComponent extends React.Component {

	constructor (props) {
		super(props)
		this.contexts = {}
		this._contextClasses = []
	}

	useContexts = (...args) => { this._contextClasses = this._contextClasses.concat(args)}

	render = () => {
		if (!this.html) {
			console.error('Could not find html function this the object', this)
			return ''
		}
		this.html = this.html.bind(this)
		const constextClasses = this._contextClasses.filter(context => context)

		return renderContext(constextClasses, this.contexts, this)
	}
}