import ValidatorException from '../../../../backend/utilities/ValidatorException'
import {i18n} from '../../../../backend/resources/i18n/en'

const PASSWORD = 'password'
const UNIQUE = 'unique'
const NOT_EMPTY = 'not_empty'
const EMAIL = 'email'
const USER = 'user'
const NICKNAME = 'nickname'
const NUMBER = 'number'

class Validator {

	static validatePassword(value) {
		if(!value) value = ''
		let result = value.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*\-#;?&])[A-Za-z\d@$!%*\-#;?&]{8,}$/)
		if(result === null) throw ValidatorException.create(i18n.VALIDATOR_INCORRECT_PASSWORD_FORMAT, value)
	}

	static validateEmail(value) {
		if(!value) value = ''
		let result = value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
		if(result === null) throw ValidatorException.create(i18n.VALIDATOR_INVALID_EMAIL_FORMAT, value)
	}

	static validateUnique(value) {

	}

	static validateUser(value) {
		if(!value) value = ''
		if(!value.match(/^[\w\d]{3,}$/)) throw ValidatorException.create(i18n.VALIDATOR_INVALID_USER_FORMAT, value)
	}

	static validateNickname(value) {
		if(!value) value = ''
		if(!value.match(/^[\w_\-.\d]{3,}$/)) throw ValidatorException.create(i18n.VALIDATOR_INVALID_NICKNAME_FORMAT, value)
	}

	static validateNotEmpty(value) {
		value = value + ''
		if(!value) throw ValidatorException.create(i18n.VALIDATOR_INVALID_EMAIL_FORMAT, value)
	}

	static validateNumber(value) {
		if(!value) value = ''
		if(!value.match(/\d+/)) throw ValidatorException.create(i18n.VALIDATOR_INVALID_EMAIL_FORMAT, value)
	}

	static validate(value, rules) {
		if('string' === typeof rules) rules = [rules]
		if('object' === typeof rules)
			Object.values(rules).forEach(rule => {
				switch (rule) {
				case NOT_EMPTY:
					Validator.validateNotEmpty(value)
					break
				case PASSWORD:
					Validator.validatePassword(value)
					break
				case NICKNAME:
					Validator.validateNickname(value)
					break
				case USER:
					Validator.validateUser(value)
					break
				case UNIQUE:
					Validator.validateUnique(value)
					break
				case EMAIL:
					Validator.validateEmail(value)
					break
				case NUMBER:
					Validator.validateNumber(value)
					break
				}
			})
	}

}


export default Validator

export {PASSWORD, UNIQUE, USER, NOT_EMPTY, EMAIL, NUMBER, NICKNAME}