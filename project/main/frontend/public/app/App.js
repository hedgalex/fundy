import React  from 'react'
import Header from './components/header/Header'
import Body from './components/content/Body'
import Content from './components/content/Content'
import Footer from './components/footer/Footer'
import { UserProvider } from './components/contexts/UserContext'
import { RestProvider } from './components/contexts/RestContext'
import { MessageProvider } from './components/contexts/MessageContext'
import { HeaderProvider } from './components/contexts/HeaderContext'
import Messages from './libs/Messages'
import Main from './pages/Main'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import RestorePassword from './pages/RestorePassword'
import NewPassword from './pages/NewPassword'

class App extends React.Component {

	constructor(props) {
		super(props)
	}

	render = () => (
		<Router>
			<RestProvider>
				<UserProvider user={this.props.user}>
					<MessageProvider>
						<Body>
							<Messages />
							<HeaderProvider>
								<Header/>
							</HeaderProvider>
							<Content>
								<Route path="/" exact component={Main} />
								<Route path="/forgot" component={RestorePassword} />
								<Route path="/change" component={NewPassword} />
							</Content>
							<Footer>
							</Footer>
						</Body>
					</MessageProvider>
				</UserProvider>
			</RestProvider>
		</Router>
	)
}

export default App