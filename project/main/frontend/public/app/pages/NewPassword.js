import React  from 'react'
import NewPasswordForm from '../components/user/NewPasswordForm'


class NewPassword extends React.Component {

	constructor (props) {
		super(props)
	}

	render () {
		return <div id="restore-password-container">
			<h1>New password</h1>
			<p>
				Input new password
			</p>
			<div id="restore-password-inner">
				<NewPasswordForm  />
			</div>
		</div>
	}
}

export default NewPassword