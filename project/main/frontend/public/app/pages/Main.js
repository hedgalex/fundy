import React  from 'react'
import UserContext from '../components/contexts/UserContext'
import ReactComponent from '../libs/ReactComponent'
import {Button} from 'semantic-ui-react'

class Main extends ReactComponent {

	constructor (props) {
		super(props)

		this.useContexts(UserContext)
		this.onClick = this.onClick.bind(this)
	}

	onClick = () => {
		const { user } = this.contexts
		if(!user.state.user) user.login({"user_id":"12","username":"admin","email":"hedgalex@gmail.com","nickname":"Admin","avatar":null})
		else user.logout()
	}

	html () {
		return <div style={{height: '400px'}}>Main page. Static page <Button onClick={this.onClick}>Click me!</Button></div>
	}
}

export default Main