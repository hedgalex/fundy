import React  from 'react'
import RestorePasswordForm from '../components/user/RestorePasswordForm'

class RestorePassword extends React.Component {

	constructor (props) {
		super(props)
	}

	render () {
		return <div id="restore-password-container">
			<h1>Restore password</h1>
			<p>
				Input the email
			</p>
			<div id="restore-password-inner">
				<RestorePasswordForm  />
			</div>
		</div>
	}
}

export default RestorePassword