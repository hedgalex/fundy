import React  from 'react'
import {Icon} from 'semantic-ui-react'
import FormContext from './../components/contexts/FormContext'
import {PASSWORD} from '../libs/Validator'
import FormInput from './FormInput'

class FormPassword extends React.Component {

	static contextType = FormContext

	constructor (props) {
		super(props)
		this.changePasswordVisibility = this.changePasswordVisibility.bind(this)
		this.state = {peek: false}
	}

	changePasswordVisibility() {
		this.setState({peek: !this.state.peek})
	}

	render () {

		let icon = this.state.peek ? 'eye slash': 'eye'
		let type = this.state.peek ? 'text': 'password'

		return <div className="fundy password">
			<FormInput iconPosition="left" icon="lock" type={type} fluid placeholder="Password" validate={{PASSWORD}} {...this.props} />
			<Icon className="fundy peek" name={icon} onMouseDown={this.changePasswordVisibility} />
		</div>
	}

}

export default FormPassword