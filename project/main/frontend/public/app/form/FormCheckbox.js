import React  from 'react'
import { Checkbox } from 'semantic-ui-react'
import FormContext from './../components/contexts/FormContext'
import {ACTION_REGISTRATE} from './FormValidator'

class FormCheckbox extends React.Component {

	static contextType = FormContext

	constructor (props) {
		super(props)
		this.onChange = this.onChange.bind(this)
		this.getValue = this.getValue.bind(this)
		this.state = {
			invalid: false,
			value: null
		}
	}

	componentDidMount() {
		if(this.context !== null) this.context.dispatch({type: ACTION_REGISTRATE, component: this})
	}

	onChange (e) {
		this.setState({value: e.target.value, invalid: false})
	}

	getValue() {
		return this.state.value
	}

	render () {
		return <Checkbox {...this.props} onChange={this.onChange} />
	}

}

export default FormCheckbox