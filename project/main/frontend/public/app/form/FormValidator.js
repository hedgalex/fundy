import React, {useReducer} from 'react'
import {Form} from 'semantic-ui-react'
import {FormProvider} from './../components/contexts/FormContext'
import Validator from '../libs/Validator'

const ACTION_REGISTRATE = 'registrate'

const reducer = (state, action) => {
	if (action.type === ACTION_REGISTRATE) {
		const {component} = action
		if(!component) return state
		let found = state.fields.find(item => item === component)
		if(found) return state
		state.fields.push(component)
	}
	return state
}

/**
 * @param {function} props.onSubmit
 * @param {object} props.children
 * */
function FormValidator(props) {

	const [state, dispatch] = useReducer(reducer, {fields: []})
	const _onSubmit = props.onSubmit
	const onSubmit = (event, props) => {
		if('function' !== typeof _onSubmit) return
		let result = true
		const data = {}
		state.fields.forEach(field => {
			const value = field.getValue()
			const rules = field.props.validate
			const name = field.props.name

			if(!name) return
			if(rules) {
				try {
					Validator.validate(value, rules)
				} catch (e) {
					field.setState({invalid: true, message: e.message})
					result = false
				}
			}

			data[name] = value
		})

		if(result) _onSubmit(event, props, data)
	}

	return <Form {...props} onSubmit={onSubmit}>
		<FormProvider value={{state, dispatch}}>{props.children}</FormProvider>
	</Form>

}

export {ACTION_REGISTRATE}
export default FormValidator