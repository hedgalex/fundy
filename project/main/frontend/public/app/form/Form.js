import React  from 'react'

class LoginForm extends React.Component {

	constructor (props) {
		super(props)
		this.onSubmit = this.onSubmit.bind(this)
	}

	collectData() {
		return {}
	}

	validate (data) {

	}

	send(data) {

	}

	onSubmit () {
		if(this.state.loading) return

		let data = this.collectData()
		if(this.validate(data)) this.send(data)
	}
}

export default LoginForm