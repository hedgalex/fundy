import React  from 'react'
import { Form } from 'semantic-ui-react'
import FormContext from './../components/contexts/FormContext'
import {ACTION_REGISTRATE} from './FormValidator'

class FormInput extends React.Component {

	static contextType = FormContext

	constructor (props) {
		super(props)
		this.onChange = this.onChange.bind(this)
		this.getValue = this.getValue.bind(this)
		this.state = {
			invalid: false,
			message: null,
			value: null
		}
	}

	componentDidMount() {
		if(this.context !== null) this.context.dispatch({type: ACTION_REGISTRATE, component: this})
	}

	onChange (e) {
		this.setState({value: e.target.value, invalid: false})
	}

	getValue() {
		return this.state.value
	}

	render () {

		let error = false

		if(this.state.invalid)
			if(this.state.message) error = {content: this.state.message, pointing: 'below'}
			else error = true

		return <Form.Input {...this.props} onChange={this.onChange} error={error}>
			{this.props.children}
		</Form.Input>
	}

}

export default FormInput