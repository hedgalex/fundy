import {Message} from 'semantic-ui-react'
import React from 'react'

class FormMessage extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			error: false
		}
	}

	render() {

		let message = this.props.message

		let messageContainerClassName = 'fundy message-container transition normal'
		if(message) messageContainerClassName = `${messageContainerClassName} display`

		return <div className={messageContainerClassName}>
			<Message className="auth-error transition slow" error header="Error" content={message} />
		</div>
	}
}

export default FormMessage

