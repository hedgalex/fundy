$(document).ready(() => {

	$('form').on('submit', function (e) {
		e.preventDefault()

		$.ajax({
			url: '/auth',
			method: 'post',
			data: {username: $('input[name=username]').val(), password: $('input[name=password]').val()},
			complete: (data) => {
				$('#output').html(data.responseText)
			}
		})

	})

	$('#status').on('click', function(e) {
		e.preventDefault()

		$.ajax({
			url: '/status',
			method: 'get',
			complete: (data) => {
				$('#output').html(data.responseText)
			}
		})

	})


})