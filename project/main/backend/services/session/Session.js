import config from '../../config'
import { sign, generateId } from '../../utilities/crypt/crypt'

const SECRET = config.cookie.secret

export default class Session {

	id
	started
	updated
	user
	cookieId
	userAgent

	static create = cookieId => new Session(cookieId)

	constructor (cookieId) {
		this.cookieId = cookieId ? cookieId: generateId(SECRET)
		this.id = sign(this.cookieId, SECRET)
		this.started = new Date().getTime()
		this.updated = this.started
	}

	update = () => this.updated = new Date().getTime()

	restore = ({ started, user, userAgent }) => {
		this.started = started
		this.user = user
		this.userAgent = userAgent
	}

}