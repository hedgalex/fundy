import Exception from '../../utilities/Exception'

export default class SessionException extends Exception {

	constructor (message, sessionId) {
		super(message)
		this.sessionId = sessionId
	}

	static create(message, sessionId) {
		return new SessionException(message, sessionId)
	}

}