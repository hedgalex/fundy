import Logger from '../../utilities/logger/Logger'
import Session from './Session'
import Request from '../web/Request'
import Response from '../web/Response'
import dataService from '../data/DataService'
import config from '../../config'
import SessionException from './SessionException'

const logger = Logger.create('SessionComponent')

// const invalidateOutdated = () => {}

class SessionService {

	/**
	 * @param { Request } request
	 * @param { User } user
	 * @throws { SessionException }
	 * */
	registerUser = async (request, user) => {
		const { session } = request
		if(!session) throw SessionException.create('Could not find session', null)

		session.user = user
		await dataService.saveSession(session)
	}

	/** @param { Request } request */
	unregisterUser = async request => {
		const { session } = request
		if(!session) throw SessionException.create('Could not find session', null)

		delete session.user
		await dataService.saveSession(session)
	}

	filter = () => async (request, response, next) => {

		const fRequest = Request.create(request),
			fResponse = Response.create(response)

		const { session } = fRequest
		const sessionData = await dataService.loadSession(session.id)
		if (sessionData) {
			session.restore(sessionData)
			session.update()
			return next()
		}

		fResponse.session = session
		await dataService.saveSession(session)

		return next()
	}

}

const sessionService = new SessionService()
export default sessionService