import Request from './Request'
import Response, { CONTENT_TYPE_JSON } from './Response'
import ReactDomServer from 'react-dom/server'
import serialize from 'serialize-javascript'
import React from 'react'
import Logger from '../../utilities/logger/Logger'
import Main from '../../content/Main'

const logger = Logger.create(__filename)

class WebService {

	filter = () => async (request, response, next) => {
		const { accept = '' } = request.headers
		if (accept.match(/(text\/html|application\/xhtml\+xml|application\/xml)/i) === null) {
			return next()
		}

		const fRequest = Request.create(request),
			fResponse = Response.create(response)

		const isRest = fRequest.getHeader('x-fundy-json')
		const { user } = fRequest
		const data = { user, isRest }
		let content = ''
		if (isRest) {
			fResponse.contentType = CONTENT_TYPE_JSON
			content = WebService.json(data)
		} else {
			content = WebService.render(data)
		}

		fResponse.send(content)
	}

	static render = data => {
		const initData = serialize(data)
		const script = `<script type="text/javascript">
			window.__INITIAL_STATE__ = ${initData}
		</script>`

		const content = ReactDomServer
			.renderToStaticMarkup(<Main {...data} />)
			.replace(/%initialState%/, script)

		return `<!DOCTYPE html>${content}`
	}

	static json = data => {
		const html = ReactDomServer.renderToStaticMarkup(<Main {...data} />)
		const head = html.replace(/[\s\S]*?%head%([\s\S]*?)%\/head%[\s\S]*/i, '$1')
		const content = html.replace(/[\s\S]*?%content%([\s\S]*?)%\/content%[\s\S]*/i, '$1')

		return JSON.stringify({ head, content })
	}
}


const webService = new WebService()
export default webService