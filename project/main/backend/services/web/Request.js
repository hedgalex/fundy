import Logger from '../../utilities/logger/Logger'
import config from '../../config'
import Session from '../session/Session'

const logger = Logger.create(__filename)
const COOKIE_NAME = config.cookie.name
const cookieRegex = new RegExp(`${COOKIE_NAME}=([\\da-f]{64})`)

const getUserAgent = request => {
	const { headers } = request
	return headers && headers['user-agent'] ? headers['user-agent']: null
}

const getCookieSessionId = request => {
	const { headers } = request

	if(!headers || !headers.cookie) return null
	const { cookie } = headers

	let matches = cookie.match(cookieRegex)
	if(!matches || matches.length < 2) return null

	return matches[1]
}

export default class Request {

	static create = request => new Request(request)

	constructor (request) {
		this.origin = request
	}

	/** @return { Session } */
	get session() {
		if(!this.origin.session) {
			const cookieId = getCookieSessionId(this.origin)
			const userAgent = getUserAgent(this.origin)
			const session = Session.create(cookieId)
			session.userAgent = userAgent
			this.origin.session = session
		}

		return this.origin.session
	}

	/** @param { Session } session */
	set session(session) {
		this.origin.session = session
	}

	get query() {
		return this.origin.query
	}

	get body() {
		return this.origin.body
	}

	get params() {
		logger.debug(this.origin)
		return this.origin.params
	}

	get user() {
		const { session = { user: false } } = this.origin
		return session.user
	}

	get isLogged() {
		const { session } = this.origin
		return session && session.user
	}

	get headers() {
		const { headers = {} } = this.origin;
		return headers
	}

	getHeader = name => this.headers[name] ? this.headers[name]: null

}