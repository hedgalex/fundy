import config from '../../config'

const COOKIE_NAME = config.cookie.name
export const
	CONTENT_TYPE_HTML = 'text/html',
	CONTENT_TYPE_JSON = 'application/json';


export default class Response {

	static create = response => new Response(response)

	constructor (response) {
		this.origin = response
		this.message = ''
		this.content = ''
	}

	/** @param { Session } session */
	set session(session) {
		const { cookieId } = session
		if(cookieId) this.origin.set('Set-Cookie', `${COOKIE_NAME}=${cookieId}`)
	}

	set contentType(contentType) {
		this.origin.set('Content-Type', contentType)
	}

	get session() {
		return this.origin.get('Set-Cookie')
	}

	setStatus = status => {
		this.origin.status(status)
		return this
	}

	setContent = content => {
		this.content = content
		return this
	}

	/** @param {String | Object} [content] */
	send = (content) => this.origin.send(content ? content: this.content)

}