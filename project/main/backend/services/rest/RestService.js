import url from 'url'
import Request from '../web/Request'
import Response from '../web/Response'
import Auth from '../../rest/api/Auth'
import Logger from '../../utilities/logger/Logger'

const modules = [Auth]

const logger = Logger.create(__filename)

class RestService {

	constructor () {
		this.modules = {}

		modules.map(restClass => {
			const methods = Object.getOwnPropertyNames(restClass.prototype)
			methods.map(name => {
				const method = restClass.prototype[name]
				const { route } = method
				if (route && !route.disabled) {
					const { path } = method.route
					if (path) {
						this.modules[path] = Object.assign(method, method.route)
					}
				}
			})
		})
	}

	filter = () => (async (request, response, next) => {
		const urlParts = url.parse(request.url, true)
		const module = this.modules[urlParts.pathname]
		const
			fRequest = Request.create(request),
			fResponse = Response.create(response)

		if(!module) return next()

		const hasMethod = request.method.localeCompare(module.type, undefined, { sensitivity: 'accent' }) === 0
		const { disabled } = module
		if(!hasMethod || disabled) return next()

		// const body = request.getBody()
		// if(!body) return response.setMessage(i18n.AUTH_ERROR_NO_USER).setStatus(400)

		await module(fRequest, fResponse)
		fResponse.send()
	})

}

const restService = new RestService()
export default restService
