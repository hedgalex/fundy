import { promisify } from 'util'
import redis from 'redis'
import Logger from '../../utilities/logger/Logger'
import config from '../../config'
import RedisException from './RedisException'

const logger = Logger.create(__filename)

const client = redis.createClient(config.redis.port, config.redis.name, {auth_pass: config.redis.pass})
const getData = promisify(client.get).bind(client)

class RedisService {

	/** @throws { RedisException } */
	setData = async (key, data) => {
		if(!key) {
			logger.warn('No key found')
			return null
		}

		const preparedData = 'object' === typeof data ? JSON.stringify(data): '' + data
		if(await client.set(key, preparedData) === false) throw RedisException.create('Could not save data')
	}

	getData = async key => {
		const data = await getData(key)
		if (typeof data === 'string')
			try {
				return JSON.parse(data)
			} catch (e) {
				logger.info('Could not parse data', key, data)
				return data
			}
		else {
			logger.info('Data not found by key', key)
			return null
		}
	}

}

const redisComponent = new RedisService()
export default redisComponent