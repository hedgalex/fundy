import Exception from '../../utilities/Exception'

export default class RedisException extends Exception {

	static create = message => new RedisException(message)

}