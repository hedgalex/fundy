import Logger from '../../utilities/logger/Logger'
import redisService from '../redis/RedisService'
import dbManager from '../../db/DBHelper'
import Session from '../session/Session'

const logger = Logger.create('DataService.js')
const SESSION_KEY = 'session'

class DataService {

	loadSession = async sessionId => {
		if(!sessionId) return null

		const sessionFromMemory = await redisService.getData(`${SESSION_KEY}:${sessionId}`)
		if(sessionFromMemory != null) return sessionFromMemory

		//restore session from db
		const sessionFromDatabase = await dbManager.sessions.get(sessionId)

		// const userData = await dbHelper.users.getFullUserById(sessionData.user_id)
		// if(!userData || !userData.user_id) {
		// 	logger.error('UserData by userId', sessionData.user_id, userData)
		// 	return false
		// }



	}

	/** @param { Session } session  */
	saveSession = async session => {
		if(!session) return

		try {
			await redisService.setData(`${SESSION_KEY}:${session.id}`, session)
		} catch (e) {
			logger.error('Could not store session', e)
		}

	}


}

const dataService = new DataService()
export default dataService