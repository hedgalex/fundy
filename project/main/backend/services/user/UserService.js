import User from '../../db/entities/User'
import dbManager from '../../db/DBHelper'

const { users } = dbManager

class UserService {

	/** @param { User } username */
	getUserByName = async username => users.getUserByName(username)

	async getUserById(id) {

		/** @typedef {String} user.file_path  */
		/** @typedef {String} user.file_url  */
		let user = await users.getFullUserById(id)
		if(!user) return false

		if(user.file_path) user.file_url = `/images/${user.file_path}`
		delete user.file_path

		return user
	}

	async createUser(username, password, nickname, email) {

		// const user = await dbManager.users.create(username, password, nickname, email)
		// if(!user) return Result.createError(i18n.REG_ERROR_COULD_NOT_CREATE_USER)
		//
		// return Result.createResponse(i18n.REG_USER_CREATED, {username: username, email: email})
	}

}

const userComponent = new UserService()
export default userComponent