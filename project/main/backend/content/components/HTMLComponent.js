import React from 'react'
import Logger from '../../utilities/logger/Logger'
import { HeadProvider } from '../context/HeadContext'
import RequestContext from '../context/RequestContext'

const logger = Logger.create(__filename)

export default class HTMLComponent extends React.Component {

	static contextType = RequestContext

	constructor(props) {
		super(props);

		if (!this.render) {
			logger.warn(this.__proto__, 'render is an arrow function or does not exists');
			this.childRender = () => ('')
		} else {
			this.childRender = this.render
		}

		this.render = this.#_render
	}

	#_render = () => {
		const { context = { isRest: false } } = this
		const content = this.#_getContent()
		return context.isRest ? this.renderJSON(content): this.renderPage(content)
	}

	#_getContent = () => {
		const content = this.childRender()
		const { props = { children: [] } } = content
		const head = props.children.find(
			item => {
				const { type = { isHeadComponent: false } } = item
				return type.isHeadComponent
			}
		);

		return { content, head }
	}

	renderJSON = ({ head, content }) => {
		return (
			<>
				%head%<HeadProvider>{ head }</HeadProvider>%/head%
				%content%{ content }%/content%
			</>
		)
	}

	renderPage = ({ head, content }) => {
		return (
			<html lang="en">
				<HeadProvider>{ head }</HeadProvider>
				<body>
					<div id="content" style={{ display: 'none' }}>{ content }</div>
					<div id="app" />
					%initialState%
					<script type="text/javascript" src={'main.js'} />
				</body>
			</html>
		)
	}

}