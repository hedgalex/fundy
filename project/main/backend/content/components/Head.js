import React from 'react'
import HeadContext from '../context/HeadContext'

class Head extends React.Component {

	static contextType = HeadContext
	static isHeadComponent = true

	render() {
		const { context, props } = this
		if (!context) return ''

		return <head>
			<meta charSet="UTF-8" />
			{ props.children }
		</head>
	}
}

export default Head