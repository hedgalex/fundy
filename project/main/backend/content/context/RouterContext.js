import React, { useState } from 'react'

const RouterContext = React.createContext(false)

export default RouterContext
export const RouterConsumer = RouterContext.Consumer

export function RouterProvider(props) {

	const { uri } = props

	const [ state ] = useState({ uri: uri })

	return (
		<RouterContext.Provider value={ state }>
			{props.children}
		</RouterContext.Provider>
	)
}