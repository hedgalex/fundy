import React from 'react'

const RequestContext = React.createContext(false)
export default RequestContext

export const RequestProvider = (props) => {
	const { user, isRest } = props
	return (
		<RequestContext.Provider value={{ user, isRest }}>
			{props.children}
		</RequestContext.Provider>
	)
}