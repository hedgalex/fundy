import React from 'react'

const HeadContext = React.createContext(false)
export default HeadContext

export function HeadProvider(props) {
	return (
		<HeadContext.Provider value={true}>
			{ props.children }
		</HeadContext.Provider>
	)
}