import React  from 'react'
import HTMLComponent from '../components/HTMLComponent'
import Head from '../components/Head'

export default class Info extends HTMLComponent {

	render = () => <div>
		<Head>
			<title>Info page</title>
			<meta name="info" content="Info" />
		</Head>

		This is info page
	</div>
}