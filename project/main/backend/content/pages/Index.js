import React from 'react'
import Head from '../components/Head'
import HTMLComponent from '../components/HTMLComponent'
import Logger from '../../utilities/logger/Logger'

const logger = Logger.create(__filename)

export default class Index extends HTMLComponent {
	render() {
		return (
			<div>
				<Head>
					<title>Index</title>
				</Head>

				This is the SUPER page
			</div>
		)
	}
}