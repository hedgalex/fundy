import React, { PureComponent } from 'react'
import RouterContext from '../context/RouterContext'
import pathToRexep from 'path-to-regexp'

export default class Route extends PureComponent {

	static contextType = RouterContext

	render = () => {
		const { isDefault, page, path, children } = this.props
		const { uri } = this.context

		const regex = pathToRexep(path, [], {})
		if(uri.match(regex)) {
			if (page) return new page().render()
			return children
		}

		return ''
	}


}