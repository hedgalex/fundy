import React from 'react'
import { RouterProvider } from './context/RouterContext'
import { RequestProvider } from './context/RequestContext'

import Route from './routes/Route'
import Info from './pages/Info'
import NewPassword from './pages/NewPassword'
import Error404 from './pages/Error404'
import Index from './pages/Index'

export default class Main extends React.Component {
	render = () => {

		const { request = {} } = this.props
		const { user = {}, isRest = false } = request

		return (
			<RequestProvider user={ user } isRest={ isRest }>
				<RouterProvider uri="/">
					<Route path="/" isDefault><Index /></Route>
					<Route path="/info"><Info /></Route>
					<Route path="/newpassword*" page={NewPassword} />
					<Route path="/404" page={Error404} />
				</RouterProvider>
			</RequestProvider>
		)
	}

}