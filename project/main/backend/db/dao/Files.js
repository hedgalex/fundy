const TABLE = 'files'

export const Files = {
	TABLE: (fieldName) => {
		if(!fieldName) return TABLE
		return `${TABLE}.${fieldName}`
	},
	id: 'id',
	name: 'name',
	path: 'path',
	url: 'url'
}