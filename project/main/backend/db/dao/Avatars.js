const TABLE = 'avatars'

export const Avatars = {
	TABLE: (fieldName) => {
		if(!fieldName) return TABLE
		return `${TABLE}.${fieldName}`
	},
	id: 'id',
	userId: 'user_id',
	fileId: 'file_id'
}