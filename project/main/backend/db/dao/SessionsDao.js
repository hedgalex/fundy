import Logger from '../../utilities/logger/Logger'
import Session from '../entities/Session'

const logger = Logger.create('Sessions.js')

let knex

class SessionsDao {

	/**
	 * @param { Knex } client
	 * @return { SessionsDao }
	 * */
	static create (client) {
		knex = client
		return new SessionsDao()
	}

	/**
	 * @param { String } sessionId
	 * @return { Object }
	 * */
	get = async sessionId => {
		if (!sessionId) return null
		return knex(Session.TABLE()).where(Session.sessionId, '=', sessionId).first()
	}

	async create(userId, sessionId, cookieId, agent) {

		let insertData = {}
		insertData[Session.userId] = userId
		insertData[Session.sessionId] = sessionId
		insertData[Session.cookieId] = cookieId
		insertData[Session.agent] = agent
		insertData[Session.sessionCreated] = new Date().toISOString()
		let result = await knex(Session.TABLE()).insert(insertData)
		return !!result
	}

	async delete (id) {
		let users = await this.get(id)
		if (users && users.length) return false

	}

}

export default SessionsDao
export {Session}