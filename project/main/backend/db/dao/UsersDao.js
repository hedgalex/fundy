import { hash } from '../../utilities/crypt/crypt'
import { Avatars } from './Avatars'
import { Files } from './Files'
import config from '../../config'
import { i18n } from '../../resources/i18n/en'
import Exception from '../../utilities/Exception'
import User from '../entities/User'

let knex

const toIndexMail = (email) => {
	return email.replace(/\./g, '')
}


class UsersDao {

	/**
	 * @param { Knex } client
	 * @return { UsersDao }
	 * */
	static create = client => {
		knex = client
		return new UsersDao()
	}

	/**
	 * @param {Number} id
	 * @return {Object|boolean|undefined}
	 * */
	async getById(id) {
		if (!id) return false

		return knex
			.select()
			.from(User.TABLE())
			.where(User.id, '=', id)
			.first()
	}

	/** @return { User } */
	getUserByName = async username => {
		if (!username) return null

		const user = await knex(User.TABLE())
			.where(User.TABLE(User.username), '=', username)
			.first()

		if(!user) return null
		return User.create(user)
	}

	/**
	 * @param {Number} id
	 * @return {Object|boolean|undefined}
	 * */
	async getFullUserById(id) {
		if (!id) return false

		return knex
			.select(
				`${User.TABLE(User.id)} as user_id`,
				`${User.TABLE(User.username)} as username`,
				`${User.TABLE(User.email)} as email`,
				`${User.TABLE(User.nickname)} as nickname`)
			.select(knex.raw(`CASE
          WHEN ${Files.TABLE(Files.path)} IS NOT NULL THEN CONCAT('${config.avatarUrl}', '/', ${Files.TABLE(Files.path)}) 
          ELSE ${Files.TABLE(Files.url)} 
          END 
        as avatar`
			))
			.from(User.TABLE())
			.where(User.TABLE(User.id), '=', id)
			.leftJoin(Avatars.TABLE(), User.TABLE(User.id), Avatars.TABLE(Avatars.userId))
			.leftJoin(Files.TABLE(), Avatars.TABLE(Avatars.fileId), Files.TABLE(Files.id))
			.first()
	}

	/**
	 * @param {String} username
	 * @param {String} [hash]
	 * @return {Object|boolean|undefined}
	 * */
	async findUser(username, hash) {
		if (!username) return false

		const condition = {}
		condition[User.username] = username
		if(hash) condition[User.hash] = hash

		return knex
			.select('id')
			.from(User.TABLE())
			.where(condition)
			.first()
	}

	async userExists(username, nickname, email) {
		const cmail = toIndexMail(email)
		return knex
			.select()
			.from(User.TABLE())
			.where(User.username, 'ILIKE', username)
			.orWhere(User.nickname, 'ILIKE', nickname)
			.orWhere(User.cmail, 'ILIKE', cmail)
			.first()
	}

	async create (username, password, nickname, email) {

		const users = await this.userExists(username, nickname, email)
		if (users) throw Exception.create(i18n.REG_ERROR_USER_EXISTS)

		let insertData = {}
		insertData[User.username] = username.toLowerCase()
		insertData[User.hash] = hash(password)
		insertData[User.email] = email
		insertData[User.cmail] = toIndexMail(email)
		insertData[User.nickname] = nickname
		insertData[User.createdOn] = new Date().toISOString()

		return knex(User.TABLE()).insert(insertData)
	}

	async delete(username) {
		let users = await this.findUser(username)
		if (users && users.length) return false


	}

}

export default UsersDao