let knex

const TABLE = 'logs'

const Log = {
	TABLE: (fieldName) => {
		if(!fieldName) return TABLE
		return `${TABLE}.${fieldName}`
	},
	userId: 'user_id',
	visitTime: 'visit_time'
}

class Logs {

	/**
	 * @param {Knex} client
	 * @return {Logs}
	 * */
	static factory (client) {
		knex = client
		return new Logs()
	}

	/**
	 * @param {String|Number} userId
	 * @return {Object|boolean}
	 * */
	async get (userId) {
		if (!userId) return false

		return knex.select('*').
			from(Log.TABLE()).
			where(Log.userId, '=', userId)
	}

	async create (userId) {

		let insertData = {}
		insertData[Log.userId] = userId
		insertData[Log.visitTime] = new Date().toISOString()
		return knex(Log.TABLE()).insert(insertData)
	}

	async delete (id) {
		let users = await this.get(id)
		if (users && users.length) return false

	}

}

export default Logs