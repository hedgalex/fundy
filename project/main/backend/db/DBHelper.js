import knex from 'knex'
import UsersDao from './dao/UsersDao'
import SessionsDao from './dao/SessionsDao'
import config from '../config'

const client = knex(config.knex)

/** @typedef UsersDao */

const users = UsersDao.create(client)
/** @typedef SessionsDao */
const sessions = SessionsDao.create(client)

class DBHelper {

	/** @type UsersDao */
	users = users

	/** @type SessionsDao */
	sessions = sessions

}

const dbManager = new DBHelper()
export default dbManager