const TABLE = 'users'

export default class User {

	static create = user => new User(user)

	static TABLE = fieldName => {
		if(!fieldName) return TABLE
		return `${TABLE}.${fieldName}`
	}

	static id = 'id'
	static username = 'username'
	static nickname = 'nickname'
	static hash = 'hash'
	static email = 'email'
	static cmail = 'cmail'
	static createdOn = 'created'

	id
	username
	nickname
	hash
	email
	cmail
	createdOn

	constructor ({id, username, nickname, hash, email, cmail, createdOn}) {
		this.id = id
		this.username = username
		this.nickname = nickname
		this.email = email
		this.cmail = cmail
		this.createdOn = createdOn
		this.hash = hash
	}

}