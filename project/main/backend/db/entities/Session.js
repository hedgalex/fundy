const TABLE = 'sessions'

export default class Session {

	static create = session => new Session(session)

	static TABLE = fieldName => {
		if(!fieldName) return TABLE
		return `${TABLE}.${fieldName}`
	}

	static userId = 'user_id'
	static sessionId = 'session_id'
	static sessionCreated = 'session_created'
	static cookieId = 'cookie_id'
	static agent = 'agent'

	userId
	sessionId
	sessionCreated
	cookieId
	agent

	constructor ({userId, sessionId, sessionCreated, cookieId, agent}) {
		this.userId = userId
		this.sessionId = sessionId
		this.sessionCreated = sessionCreated
		this.cookieId = cookieId
		this.agent = agent
	}

}