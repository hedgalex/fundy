import { DEBUG } from './utilities/logger/LogLevel'

const config = {
	DEFAULT_PORT: 3000,
	avatarUrl: '/image',
	cookie: {
		name: 'cid',
		secret: 've-m-l-y-o',
	},
	crypt: {
		sha256_salt: '275a262958{0}2aa2af3469364c6748cf38',
		md5_salt: '9c74e172f87984c48{0}ddf5c8108cabe67',
	},
	redis: {
		port: 6379,
		name: 'redis',
		pass: '123',
	},
	knex: {
		client: 'pg',
		searchPath: ['knex', 'public'],
		connection: process.env.DATABASE_URL,
	},
	logger: {
		mode: DEBUG,
	},
}

export default config