import Route from '../Route'
import {POST} from '../methods'
import Response from '../Response'
import {i18n} from '../../../i18n/en'
import userComponent from '../../../components/UserComponent'
import Result from '../../Result'
import Validator, {EMAIL, NICKNAME, NOT_EMPTY, PASSWORD, USER} from '../../../../client/public/app/libs/Validator'
import Exception from '../../../utils/Exception'
import Logger from '../../../utils/Logger'

class Registration {

	@Route({path: '/rest/registration', method: POST, provides: 'text/json'})
	async signup(data) {

		if (!data || !data.body) return Response.setMessage(i18n.ERROR_BAD_REQUEST).setError(415)

		const { username, password, nickname, email } = data.body

		//Validation
		if (typeof username === 'undefined' || username === '') return Response.setMessage(i18n.REG_ERROR_NO_USER).setError(415)
		if (typeof password === 'undefined' || password === '') return Response.setMessage(i18n.REG_ERROR_NO_PASSWORD).setError(415)
		if (typeof email === 'undefined' || email === '') return Response.setMessage(i18n.REG_ERROR_NO_EMAIL).setError(415)

		try {

			Validator.validate(username, USER)
			Validator.validate(password, PASSWORD)
			Validator.validate(email, EMAIL)

 		} catch (e) {
			return Response.setMessage(e.message).setError(415)
		}

		let nicknameNonEmpty =  !nickname ? username: nickname
		Validator.validate(nicknameNonEmpty, NICKNAME)

		try {

			const result = await userComponent.createUser(username, password, nicknameNonEmpty, email)
			return Response.setMessage(result.responseData)

		} catch (e) {
			return Response.setMessage(e.message).setError(415)
		}

	}

}

export default Registration