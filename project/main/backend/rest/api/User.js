import endpoint from '../Route'
import {i18n} from '../../../i18n/en'
import {GET} from '../methods'
import userComponent from '../../../components/UserComponent'
import Response from '../Response'

class Auth {

	@endpoint({path: '/rest/user', method: GET, provides: 'text/json'})
	async user(data) {

		if(!data.session || !data.session.auth) {
			return Response.setMessage(i18n.USER_UNAUTHORIZED).setError(403)
		}

		let user = await userComponent.getUserById(data.session.auth.id)
		if(!user) return Response.setMessage(i18n.AUTH_UNKNOWN_ERROR).setError(403)

		return Response.setMessage(user)
	}

}

export default Auth