import Route from '../Route'
import Request from '../../services/web/Request'
import Response from '../../services/web/Response'
import Logger from '../../utilities/logger/Logger'
import { i18n } from '../../resources/i18n/en'
import userService from '../../services/user/UserService'
import sessionService from '../../services/session/SessionService'

const logger = Logger.create(__filename)

class Auth {

	/**
	 * @param { Request } request
	 * @param { Response } response
	 */
	@Route({path: '/rest/test', type: 'get', provides: 'text/json'})
	async test(request, response) {
		// logger.log('Test')
	}

	/**
	 * @param { Request } request
	 * @param { Response } response
	 * @description
	 * 200 - OK
	 * 401 Is not authorized
	 * 415 Wrond data
	 */
	@Route({path: '/rest/logout', type: 'get', provides: 'text/json'})
	async logout(request, response) {

		if(!request.isLogged()) {
			logger.warn('User is not authorized')
			return response.setContent(i18n.AUTH_USER_UNAUTHORIZED).setStatus(401)
		}

		try {
			await sessionService.unregisterUser(request)
		} catch (e) {
			logger.error(e)
			response.setContent(i18n.AUTH_UNKNOWN_ERROR).setStatus(415)
		}

		response.setContent(i18n.AUTH_LOGOUT_SUCCESSED)
	}

	/**
	 * @param { Request } request
	 * @param { Response } response
	 * @description REST api responses { id, username, avatar,  }
	 */
	@Route({path: '/rest/auth', type: 'post', provides: 'text/json'})
	async auth(request, response) {

		const { username, password, remember } = request.body
		if(request.isLogged()) {
			logger.warn('User is authorized', username)
			return response.setContent(i18n.AUTH_USER_AUTHORIZED).setStatus(400)
		}

		if(!username || !password) {
			return response.setContent(i18n.AUTH_ERROR_INSUFFICIENT_DATA).setStatus(415)
		}

		const user = await userService.getUserByName(username)
		if(!user) response.setContent(i18n.AUTH_USER_NOT_FOUND).setStatus(400)

		try {
			await sessionService.registerUser(request, user)
		} catch (e) {
			logger.error(e)
			response.setContent(i18n.AUTH_UNKNOWN_ERROR).setStatus(415)
		}

		const { id, nickname, email, createdOn, avatar } = user
		response.setContent({ id, username: user.username, nickname, email, createdOn, avatar })
	}

}

export default Auth