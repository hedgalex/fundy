import endpoint from '../Route'
import {i18n} from '../../../i18n/en'
import {GET} from '../methods'
import Response from '../Response'

class Status {

	@endpoint({path: '/rest/status', method: GET, provides: 'text/json'})
	async status(data) {
		return Response.setMessage(i18n.STATUS_OK).setStatus(200)
	}

}

export default Status