import Cookie from '../http/cookie/Cookie'

class Response {

	constructor () {
		this.dStatus = 200
		this.dError = false
		this.dMessage = ''
		this.dExtented = {}
		this.dHeaders = {}
		this.dCookie = false
	}

	static addHeader(name, value) {
		const response = new Response()
		return response.addHeader(name, value)
	}

	static setMessage(message) {
		const response = new Response()
		return response.setMessage(message)
	}

	static setStatus(status) {
		const response = new Response()
		return response.setStatus(status)
	}

	static setExtented(data) {
		const response = new Response()
		return response.setExtended(data)
	}

	static setError(status) {
		const response = new Response()
		return response.setError(status)
	}

	static setCookie(cookie) {
		const response = new Response()
		return response.setCookie(cookie)
	}

	setStatus(status) {
		this.dStatus = status
		return this
	}

	/** @return {number} */
	get status() {
		return this.dStatus
	}

	setMessage(message) {
		this.dMessage = message
		return this
	}

	/** @return {Object|string} */
	get message() {
		return this.dMessage
	}

	setError(status) {
		this.dError = true
		this.dStatus = status
		return this
	}

	/** @return {boolean} */
	get error() {
		return this.dError
	}

	setExtended (data) {
		this.dExtented = data
		return this
	}

	/** @return Object */
	get extended() {
		return this.dExtented
	}

	/** @param {Cookie} cookie */
	setCookie (cookie) {
		this.dCookie = cookie
		return this
	}

	/** @return Cookie */
	get cookie () {
		return this.dCookie
	}

	addHeader (name, value) {
		this.dHeaders[name] = value
		return this
	}

	get headers() {
		return this.dHeaders
	}
}

export default Response