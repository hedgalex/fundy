export default function Route(data) {
	return function decorator(target, name, descriptor) {

		if(!descriptor.value) console.error('Error: Annotation `Route` applied to class')
		else descriptor.value.route = data

		return target
	}
}