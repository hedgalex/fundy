const GET = 'get'
const POST = 'post'
const PUT = 'put'
const DELETE = 'delete'

export {GET, POST, DELETE, PUT}