class Exception {

	constructor(message) {
		this.message = message
	}

	static create(message) {
		return new Exception(message)
	}

}

export default Exception