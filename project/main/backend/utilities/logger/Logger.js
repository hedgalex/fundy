import colors from './LoggerScheme'
import { DEBUG, ERROR, INFO, WARN } from '../../utilities/logger/LogLevel'
import config from '../../config'
import util from 'util'

const getTime = () => new Date().toISOString().replace(/(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}:\d{2}).*$/,'[$1 $2]')
const _ = (...args) => args.map(item => util.inspect(item, { showHidden: false, depth: 0 }))
const { mode } = config.logger

export default class Logger {

	constructor (name) {
		this.source = name
	}

	static create = (name) => new Logger(name)

	static log = (...args) => console.log(colors.GREEN, colors.YELLOW, getTime(), colors.WHITE, ..._(...args))
	static info = (...args) => mode === INFO ? console.log(colors.GREEN, 'INFO:', getTime(), colors.WHITE, ..._(...args)): undefined
	static debug = (...args) => mode <= DEBUG ? console.log(colors.WHITE, 'DEBUG:', getTime(), colors.WHITE, ..._(...args)): undefined
	static warn = (...args) => mode <= WARN ? console.log(colors.YELLOW, 'WARN:', getTime(), colors.WHITE, ..._(...args)): undefined
	static error = (...args) => mode <= ERROR ? console.error(colors.RED, 'ERROR:', getTime(), colors.WHITE, ..._(...args)): undefined
	static fatal = (...args) => console.error(colors.MAGENTA, 'FATAL:', getTime(), colors.WHITE, ..._(...args))

	log = (...args) => console.log(colors.GREEN, getTime(), `[${this.source}]`, colors.WHITE, ..._(...args))
	info = (...args) => mode === INFO ? console.log(colors.WHITE, 'INFO:', getTime(), `[${this.source}]`, colors.WHITE, ..._(...args)): undefined
	debug = (...args) => mode <= DEBUG ? console.log(colors.BLUE, 'DEBUG:', getTime(), `[${this.source}]`, colors.WHITE, ..._(...args)): undefined
	warn = (...args) => mode <= WARN ? console.log(colors.YELLOW, 'WARN:', getTime(), `[${this.source}]`, colors.WHITE, ..._(...args)): undefined
	error = (...args) => mode <= ERROR ? console.error(colors.RED, 'ERROR:', getTime(), `[${this.source}]`, colors.WHITE, ..._(...args)): undefined
	fatal = (...args) => console.error(colors.MAGENTA, 'FATAL:', getTime(), `[${this.source}]`, colors.WHITE, ..._(...args))
}