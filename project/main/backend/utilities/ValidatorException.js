class ValidatorException {

	constructor(message, value) {
		this.dMessage = message ? message: 'Unknown error: {0}'
		this.dValue = value
	}

	static create(message, value) {
		return new ValidatorException(message, value)
	}

	get message() {
		// console.error(this.dMessage.replace(/{(\d+)}/g, this.dValue))
		return this.dMessage.replace(/{(\d+)}/g, this.dValue)
	}

}

export default ValidatorException