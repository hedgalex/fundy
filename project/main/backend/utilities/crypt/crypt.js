import crypto from 'crypto'
import config from '../../config'

export const hash = value => crypto.createHmac('sha256', config.crypt.sha256_salt.replace(/\{0}/g, value)).digest('hex')
export const md5 = value => crypto.createHash('md5').update(config.crypt.md5_salt.replace(/\{0}/g, value)).digest('hex')
export const sign = (value, key) => hash(hash(value) + hash(key))
export const generateId = key => hash(key + hash(new Date().toISOString()) + Math.random())