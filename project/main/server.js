import path from 'path'
import fs from 'fs'
import express from 'express'
import config from './backend/config'
import restService from './backend/services/rest/RestService'
import sessionService from './backend/services/session/SessionService'
import webService from './backend/services/web/WebService'

const port = process.env.PORT || config.DEFAULT_PORT

/**
 * @typedef app
 * @typedef app.use
 * @typedef app.listen
 * @typedef app.static
 * @typedef app.listen
 * @typedef express.urlencoded
 */
const app = express()

app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.use(sessionService.filter())
app.use(restService.filter())
app.use('/', webService.filter())

app.use('/', express.urlencoded({ extended: true }))
app.use('/image', express.static(path.join('.', 'files')))
app.use('/', express.static(path.join('.', 'dist', 's')))

// fs.readdirSync('./dist/s/').forEach(file => {
// 	console.log(file);
// });

app.listen(port, () => console.log(`Ready ... ${port}`))