const path = require('path')
// const HtmlWebPackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const fs = require('fs')
const WebpackShellPlugin = require('webpack-shell-plugin')

const
	serverPathDist = path.resolve(__dirname, 'dist'),
	clientPathDist = path.resolve(__dirname, 'dist', 's'),
	clientPathSrc = './main/frontend/public/app.js',
	serverPathSrc = './main/server.js'

const clientConfig = {
	entry: {
		app: clientPathSrc
	},
	output: {
		filename: 'main.js',
		path: clientPathDist,
	},
	mode: 'development',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			// {
			// 	test: /\.html$/,
			// 	use: [
			// 		{
			// 			loader: 'html-loader'
			// 		},
			// 	],
			// },
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'fonts/'
						}
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: ['file-loader'],
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin()
		// new HtmlWebPackPlugin({
		// 	template: './main/client/public/index.html',
		// 	filename: './index.html',
		// 	excludeChunks: ['server']
		// }),
	],
}

//Server
const nodeModules = {}
fs.readdirSync('node_modules')
	.filter(function(x) {
		return ['.bin'].indexOf(x) === -1
	})
	.forEach(function(mod) {
		nodeModules[mod] = 'commonjs ' + mod
	})

const serverConfig = {
	entry: {
		server: serverPathSrc
	},
	target: 'node',
	output: {
		path: serverPathDist,
		filename: 'server.js',
		publicPath: '/'
	},
	mode: 'development',
	externals: nodeModules,
	module: {
		rules: [
			{
				test: /\.(jsx|js)$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.json$/,
				loader: 'json',
			},
		]
	},
	plugins: [
		new CleanWebpackPlugin()
	]
}

if (process.env.NODE_ENV !== 'production') {
	serverConfig.plugins.push(new WebpackShellPlugin({
		onBuildEnd: ['nodemon --watch ./dist/server.js --ignore ./dist/s']})
	)
}

module.exports = [ clientConfig, serverConfig ]