exports.up = function (knex) {
	return knex.schema.createTable('files', t => {
		t.bigIncrements('id').primary()
		t.string('name', 50).notNullable()
		t.string('path', 255).defaultTo(null)
		t.string('url', 355).defaultTo(null)
	})
}

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('files')
}