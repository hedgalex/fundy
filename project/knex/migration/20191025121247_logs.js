exports.up = function (knex) {
	return knex.schema.createTable('logs', t => {
		t.bigIncrements('id')
		t.bigInteger('user_id')
		t.timestamp('action')
	})
}

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('logs')
}
