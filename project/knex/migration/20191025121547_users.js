exports.up = async function (knex) {
	await knex.schema.dropTableIfExists('users')
	return knex.schema.createTable('users', t => {
		t.bigIncrements('id')
		t.string('username', 50)
		t.string('nickname', 50)
		t.string('hash', 64)
		t.string('email', 255)
		t.string('cmail', 255)
		t.boolean('confirmed').defaultTo(false)
		t.timestamp('created')
	})
}

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('users')
}