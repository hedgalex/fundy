exports.up = function (knex) {
	return knex.schema.createTable('avatars', t => {
		t.bigIncrements('id').primary()
		t.bigInteger('user_id')
		t.bigInteger('file_id')
	})
}

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('avatars')
}