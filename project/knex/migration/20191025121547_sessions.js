exports.up = function (knex) {
	return knex.schema.createTable('sessions', t => {
		t.bigIncrements('id')
		t.bigInteger('user_id')
		t.string('session_id', 128)
		t.string('cookie_id', 128)
		t.string('agent', 255)
		t.timestamp('session_created')
	})
}

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('sessions')
}
