exports.up = function (knex) {
	return knex.schema.createTable('confirmations', t => {
		t.bigIncrements('id')
		t.string('code', 64).defaultTo('')
		t.timestamp('expires')
	})
}

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('confirmations')
}
