exports.seed = function (knex) {
	return knex('users').del()
		.then(function () {
			return knex('users').insert([
				{username: 'admin', nickname: 'Admin', hash: '85dfc6ff2a37df46d32e2e76f3b9ab2f19b2cfc1540950121e6083f403509b73', email: 'hedgalex@gmail.com', cmail: 'hedgalex@gmail.com', created: '2019-10-22 02:01:36.594477'},
				{username: 'tanya', nickname: 'Tanechka', hash: 'b3d9a3e731e03d9744e066477a200b121b06a0a9f4f7c473c0052592212afb0d', email: 'tanya@email.com', cmail: 'tanya@email.com', created: '2019-10-22 10:01:36.594477'},
				{username: 'liza', nickname: 'Liza', hash: '5a2b921c9e7e957eed56d96c38b5652dde9d67e1547f11012da836dbfed0494c', email: 'liza@email.com', cmail: 'liza@email.com', created: '2019-10-22 10:01:36.594477'},
				{username: 'anna', nickname: 'Anna', hash: '2690c525f28982a6ba4f45dc2d3144ef908350a25cec1e458acd5094708e3737', email: 'anna@email.com', cmail: 'anna@email.com', created: '2019-10-22 11:01:36.594477'},
				{username: 'guest', nickname: 'Guest', hash: '58153f136822418ebbf706b24d4d578fc31aad7cb8a06b8a4e9df01b30d4bc09', email: 'guest@email.com', cmail: 'guest@email.com', created: '2019-10-22 11:01:36.594477'}
			])
		})
}