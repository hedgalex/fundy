exports.seed = function (knex) {
	return knex('avatars').del()
		.then(function () {
			return knex('avatars').insert([
				{user_id: 1, file_id: 4},
				{user_id: 2, file_id: 3},
				{user_id: 3, file_id: 2},
				{user_id: 4, file_id: 1},
				{user_id: 5, file_id: 4}
			])
		})
}