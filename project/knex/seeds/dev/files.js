exports.seed = function (knex) {
	return knex('files').del()
		.then(function () {
			return knex('files').insert([
				{id: 1, name: 'file1.jpg', path: 'anna.jpg', url: null},
				{id: 2, name: 'file2.jpg', path: 'liza.jpg', url: null},
				{id: 3, name: 'file3.jpg', path: 'tanya.jpg', url: null},
				{id: 4, name: 'file4.png', path: null, url: 'https://haruhi.techtime.org/stash/users/asheff/avatar.png'}
			])
		})
}