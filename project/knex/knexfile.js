// Update with your config settings.

module.exports = {

	development: {
		client: 'pg',
		connection: 'postgres://fundy:123@postgres:5432/fundy',
		migrations: {
			directory: './migration',
			tableName: 'knex_migrations'
		},
		seeds: {
			directory: './seeds/dev'
		}
	},

	staging: {
		client: 'pg',
		connection: {
			database: 'fundy',
			user: 'fundy',
			password: '123',
			port: 5432
		},
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			directory: './migration',
			tableName: '/knex_migrations'
		},
		seeds: {
			directory: './seeds/dev'
		}
	},

	production: {
		client: 'pg',
		connection: {
			database: 'fundy',
			user: 'fundy',
			password: '123',
			port: 5432
		},
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			directory: './migration',
			tableName: 'knex_migrations'
		},
		seeds: {
			directory: './seeds/prod'
		}
	}

}
