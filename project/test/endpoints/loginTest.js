import chai from 'chai'
import chaiHttp from 'chai-http'
import assert from 'assert'
import { i18n } from '../../main/server/i18n/en'
import RestData from '../../main/server/http/rest/RestData'
import Auth from '../../main/server/http/rest/entities/Auth'
import RestResponse from '../../main/server/http/rest/RestResponse'

chai.use(chaiHttp)
const should = chai.should()
const expect = chai.expect
const server = 'http://localhost:3000'

describe('POST /rest/auth', () => {
	it('should return \'If you want to authenticate, you must provide username\' ver 1', done => {
		chai.request(server).post('/rest/auth').end((error, response) => {
			response.should.have.status(415)
			expect(JSON.parse(response.text)).
				to.
				deep.
				equal({
					'error': true,
					'message': 'If you want to authenticate, you must provide username',
				})
			done()
		})
	})

	it('should return \'If you want to authenticate, you must provide password\'',done => {
		chai
			.request(server)
			.post('/rest/auth')
			.send({ username: 'test' })
			.end((error, response) => {
				response.should.have.status(415)
				expect(JSON.parse(response.text)).to.deep.equal({
					'error': true,
					'message': 'If you want to authenticate, you must provide password',
				})
				done()
			})
	})

	it('should return \'If you want to authenticate, you must provide username\' ver 2',done => {
		chai
			.request(server)
			.post('/rest/auth')
			.send({ username: '', password: '' })
			.end((error, response) => {
				response.should.have.status(415)
				expect(JSON.parse(response.text)).to.deep.equal({
					'error': true,
					'message': 'If you want to authenticate, you must provide username',
				})
				done()
			})
	})

	it('should return \'If you want to authenticate, you must provide username\' ver 3', done => {
		chai
			.request(server)
			.post('/rest/auth')
			.send({ password: 'test' })
			.end((error, response) => {
				response.should.have.status(415)
				expect(JSON.parse(response.text)).to.deep.equal({
					'error': true,
					'message': 'If you want to authenticate, you must provide username',
				})
				done()
			})
	})

	it('should return \'Wrong username or password\'', done => {
		chai
			.request(server)
			.post('/rest/auth')
			.send({ username: 'test', password: 'test' })
			.end((error, response) => {
				response.should.have.status(415)
				expect(JSON.parse(response.text)).to.deep.equal({
					'error': true,
					'message': 'Wrong username or password',
				})
				done()
			})
	})

	it('Simple authorization', done => {
		chai
			.request(server)
			.post('/rest/auth')
			.send({ username: 'admin', password: 'admin' })
			.end((error, response) => {
				response.should.have.status(200)
				const { text } = response
				if(!text) {
					assert.fail('Response body is empty')
					return done()
				}
				const data = JSON.parse(text)
				const { message } = data
				if(!message) {
					assert.fail('Message not found')
					return done()
				}
				expect(message.username).to.deep.equal('admin')
				expect(message.nickname).to.deep.equal('Admin')
				done()
			})
	})

	it('should return \'You are already authorized\'', done => {
		chai
			.request(server)
			.post('/rest/auth')
			.send({ username: 'admin', password: 'admin' })
			.end((error, response) => {
				response.should.have.status(200)
				let cookie = response.header['set-cookie']
				if (!cookie) {
					assert.fail('Cookie not found')
					return done()
				}

				const cookies = cookie.map(item => item.split('; ')[0])
				chai
					.request(server)
					.post('/rest/auth')
					.set('Cookie', cookies.join(';'))
					.send({ username: 'admin', password: 'admin' })
					.end((error, response) => {
						response.should.have.status(400)
						expect(JSON.parse(response.text)).to.deep.equal({
							'error': true,
							'message': 'You are already authorized'
						})
					})

			})
		done()
	})
})