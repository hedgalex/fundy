# Instalation

## Create a volume
`docker volume create --name=data`

## Create images and containers then run containers
`docker-compose up`

# Migration

`docker exec -it -w /var/project fundy npm run migrate`

# REST

`curl -i -d "username=guest&password=guest" -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:3000/auth`
